var ngRos = angular.module('ros', []);

ngRos.factory('rosConnection', ['$rootScope', function ($rootScope) {
  var ros = new ROSLIB.Ros();

  ros.connected = false;

  // If there is an error on the backend, an 'error' emit will be emitted.
  ros.on('error', function(error) {
    console.log(error);
  });

  // Find out exactly when we made a connection.
  ros.on('connection', function() {
    $rootScope.$apply(function(){
      ros.connected = true;
    });
    console.log('Connectionp made!');
  });

  // Create a connection to the rosbridge WebSocket server.
  var rosWebSocketAddress = 'ws://localhost:9090';
  if( typeof( $rootScope.rosWebSocketAddress ) !== 'undefined' ){
    rosWebSocketAddress = $rootScope.rosWebSocketAddress;
  }

  ros.connect( rosWebSocketAddress )

  return ros;
}]);



var ngRos = angular.module('ros');

ngRos.directive('rosWs', ['$rootScope',function($rootScope){
  return {
    priority: 450,
    compile: function() {
      return {
        pre: function(scope, element, attrs) {
          $rootScope.rosWebSocketAddress = attrs.rosWs;
        }
      };
    }
  }
}]);

var ngRos = angular.module('ros');


ngRos.controller('TopicListnerCtl', ['$scope', 'rosConnection', function($scope, rosConnection) {
  $scope.listener = new ROSLIB.Topic({
    ros : rosConnection,
    name : $scope.rosTopic,
    messageType : $scope.rosMessageType,
  });

  $scope.messages = [];

  $scope.listener.subscribe(function(message) {
    $scope.$apply(function(){
      $scope.messages.push(message);
      if($scope.messages.length > 10){
        $scope.messages.unshift();
      }
    });
  });
}]);


var ngRos = angular.module('ros');

ngRos.directive('rosTopic', function(){
  return {
    priority: 450,
    compile: function() {
      return {
        pre: function(scope, element, attrs) {
          scope.rosTopic = attrs.rosTopic;
        }
      };
    }
  }
});

ngRos.directive('rosMessageType', function(){
  return {
    priority: 450,
    compile: function() {
      return {
        pre: function(scope, element, attrs) {
          scope.rosMessageType = attrs.rosMessageType;
        }
      };
    }
  }
});
