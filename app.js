// Angular app
var app = angular.module('test', [ ]);

// Simple controller
app.directive('rosUrl', function(){
    return {
        restrict: 'A',
        compile: function() {
            return {
                pre: function(scope, element, attrs) {
                    //scope.rosUrl = attrs.rosUrl;
                    
                    var regex = new RegExp("[\\?&]" + 'server' + "=([^&#]*)"),
                    results = regex.exec(location.search);
                    console.log("Location: " + results[1]);
                    scope.rosUrl = results[1];
                }
            };
        }
    }
});

app.controller("initRos", ["$scope", function ($scope) {
    console.log("Register to server URL: " + $scope.rosUrl);

    $scope.ros = new ROSLIB.Ros({
        url : 'ws://' + $scope.rosUrl + ':9090'
    });

    $scope.ros.on('connection', function() {
        console.log("Connected to websocket.");
    });

    $scope.ros.on('error', function(error) {
        console.log("Error connecting to websocket server.");
    });
    $scope.ros.on('close', function() {
        console.log("Connection to websocket server closed.");
    });
}]);

app.controller("RosSubscriber", function ($scope) {
    console.log("Subscribe to topic " + $scope.rosTopic + ", of type: " + $scope.rosMessageType);

    $scope.listener = new ROSLIB.Topic({
        ros : $scope.ros,
        name : $scope.rosTopic,
        messageType : $scope.rosMessageType
    });
    $scope.topic_name = $scope.rosTopic;
    $scope.listener.subscribe(function(message) {
        $scope.$apply(function(){
            $scope.message = message;
        })
    });
});

app.directive('rosTopic', function(){
    return {
        restrict: 'A',
        compile: function() {
            return {
                pre: function(scope, element, attrs) {
                    scope.rosTopic = attrs.rosTopic;
                }
            };
        }
    }
});

app.directive('rosMessageType', function(){
    return {
        restrict: 'A',
        compile: function() {
            return {
                pre: function(scope, element, attrs) {
                    scope.rosMessageType = attrs.rosMessageType;
                }
            };
        }
    }
});

app.directive('rosStdMsgString', function(){
    return {
        restrict: 'E',
        template: "<div class='col-md-6'> <h3>{{topic_name}}: <i>{{message.data}}</i></h3> </div><hr/>"
    }
});

app.directive('rosAuvMsgsNavsts', function(){
    return {
        restrict: 'E',
        templateUrl: "./nav_sts.html",
        controller: function($rootScope){
            this.expand = true;
        },
        controllerAs: 'ctrl'
    }
});

app.directive('rosAuvMsgsBodyvelocityreq', function(){
    return {
        restrict: 'E',
        templateUrl: "./body_velocity_req.html",
        controller: function($rootScope){
            this.expand = true;
        },
        controllerAs: 'ctrl'
    }
});


app.directive('rosAuvMsgsBodyforcereq', function(){
    return {
        restrict: 'E',
        templateUrl: "./body_force_req.html",
        controller: function($rootScope){
            this.expand = true;
        },
        controllerAs: 'ctrl'
    }
});


app.directive('rosAuvMsgsWorldwaypointreq', function(){
    return {
        restrict: 'E',
        templateUrl: "./world_waypoint_req.html",
        controller: function($rootScope){
            this.expand = true;
        },
        controllerAs: 'ctrl'
    }
});


app.directive('rosDiagnosticMsgsDiagnosticArray', function(){
    return {
        restrict: 'E',
        templateUrl: "./diagnostic_array.html",
        controller: function(){
            this.expand = true;
            this.elements = [];
            this.addStatus = function(name){
                var index = this.elements.indexOf(name);
                if( index === -1 ){
                    this.elements.push(name);
                }
                else {
                    this.elements.splice(index, 1);
                }
            };
            this.checkStatus = function(name){
                if(this.elements.indexOf(name) >= 0 ){
                    return false;
                }
                else {
                    return true;
                }
            };
        },
        controllerAs: 'ctrl'
    }
});


// Calling a service
// -----------------
app.directive('rosService', function(){
    return {
        restrict: 'A',
        compile: function() {
            return {
                pre: function(scope, element, attrs) {
                    scope.rosService = attrs.rosService;
                }
            };
        }
    }
});

app.directive('rosServiceType', function(){
    return {
        restrict: 'A',
        compile: function() {
            return {
                pre: function(scope, element, attrs) {
                    scope.rosServiceType = attrs.rosServiceType;
                }
            };
        }
    }
});

app.controller("RosCreateService", ["$scope", function ($scope) {
    console.log("Call service " + $scope.rosService + ", of type: " + $scope.rosServiceType);

    $scope.service = new ROSLIB.Service({
        ros : $scope.ros,
        name : $scope.rosService,
        serviceType : $scope.rosServiceType
    });
    $scope.service_name = $scope.rosService;
}]);

app.directive("rosStdSrvsEmpty", function(){
    return {
        restrict: 'E',
        templateUrl: "empty_srvs.html",
        controller: function($scope){
            this.service = $scope.service;
            this.expand = true;
            this.callService = function(){
                var request = ROSLIB.ServiceRequest({});
                this.service.callService(request, function(result){
                    console.log('Empty service called.');
                });
            };
        },
        controllerAs: 'ctrl'
    }
});


app.directive("digitalOutputSrv", function(){
    return {
        restrict: 'E',
        templateUrl: 'digital_output_srv.html',
        controller: function($scope){
            this.service = $scope.service;
            this.expand = true;
            this.request = {};
            $scope.output = {
                text: $scope.service,
                state: true
            };
            this.callService = function(request){
                var req = new ROSLIB.ServiceRequest({
                    digital_output: parseInt(request.digital_output),
                    value: false
                });
                if( request.value ) req.value = true;
                this.service.callService(req, function(result){
                    console.log("Digital output service called. Success: " + result.success);
                    if(result.success) $scope.output.text = "Service called succesfully";
                    else $scope.output.text = "There is a problem calling the service"
                    //$scope.output.state = result.success;
                    $scope.output.text = "called with " + result.success;
                });
                
            };
            this.clear = function(){
                this.request = {};
                $scope.output = {
                    text: "erased",
                    state: true
                };
            };
        },
        controllerAs: 'ctrl'
    }
});


app.directive("rosCola2SrvGoto", function(){
    return {
        restrict: 'E',
        templateUrl: 'goto_srv.html',
        controller: function($scope){
            this.service = $scope.service;
            this.expand = true;
            this.request = {};
            $scope.output = {
                text: "",
                state: true
            };
            this.callService = function(request){
                var req = new ROSLIB.ServiceRequest({
                    north_lat: parseFloat(request.north_lat),
                    east_lon: parseFloat(request.east_lon),
                    z: parseFloat(request.z),
                    altitude_mode: request.altitude_mode,
                    tolerance: parseFloat(request.tolerance)
                });
                this.service.callService(req, function(result){
                    console.log("Goto service called. Attempted: " + result.attempted);
                    if(result.attempted) $scope.output.text = "Service called succesfully";
                    else $scope.output.text = "There is a problem calling the service"
                    $scope.output.state = result.attempted;
                });
            };
            this.clear = function(){
                this.request = {};
                $scope.output = {
                    text: "",
                    state: true
                };
            };
        },
        controllerAs: 'ctrl'
    }
});


app.directive("rosCola2Services", function(){
    return {
        restrict: 'E',
        templateUrl: 'cola2_services_v2.html'
    }
});
