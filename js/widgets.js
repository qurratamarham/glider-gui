var blue = '#0088CC';
var red = '#BD362F';
var green = '#51A351';
var orange = '#F89406';
var grey = '#808080';
var black = '#003300';

var table_width = 0;

// Drop down menu
function connect() {
    var e = document.getElementById("vehicle");
    var strUser = e.options[e.selectedIndex].value;
    window.location.href = "index.html?server=" + strUser;
}

// Take vehicle to connect
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Define canvas width and height
function resizeWidgets() {
    tmp = parseFloat(document.getElementById('table_widgets').offsetWidth);
    if( tmp != table_width ) {
        table_width = tmp;
        document.getElementById('compass_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('compass_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('horizon_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('horizon_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('velocity_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('velocity_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('depth_canvas').width = +Math.round(2*table_width/3.4);
        document.getElementById('depth_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('charge_canvas').width = +Math.round(table_width/5.5);
        document.getElementById('charge_canvas').height = +Math.round(table_width/5.5);
        document.getElementById('timeout_canvas').width = +Math.round(table_width/5.5);
        document.getElementById('timeout_canvas').height = +Math.round(table_width/5.5);
        document.getElementById('setpoints_canvas').width = +Math.round(table_width);
        document.getElementById('setpoints_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('multibeam_canvas').width = +Math.round(table_width/2.1);
        document.getElementById('multibeam_canvas').height = +Math.round(table_width/3.2);
        document.getElementById('profiler_canvas').width = +Math.round(table_width/2.1);
        document.getElementById('profiler_canvas').height = +Math.round(table_width/3.2);
        document.getElementById('sss_canvas').width = +Math.round(2*table_width/3.4);
        document.getElementById('sss_canvas').height = +Math.round(table_width/3.4);
    }
}
var interval_resize = setInterval(function(){resizeWidgets();}, 200);

// Connecting to ROS
// -----------------
var ros = new ROSLIB.Ros();


// If there is an error on the backend, an 'error' emit will be emitted.
ros.on('error', function(error) {
    console.log(error);
});

// Create a connection to the rosbridge WebSocket server.
var server_name = 'ws://' + getParameterByName('server') + ':9090';
console.log("connect to server: " + server_name );
ros.connect(server_name);


// NAVIGATION
var listener = new ROSLIB.Topic({
    ros : ros,
    name : '/cola2_navigation/nav_sts_hz',
    messageType : 'auv_msgs/NavSts'
});

listener.subscribe(function(message) {
    drawCompass(message.orientation.yaw*180/3.14159);
    drawHorizon(message.orientation.roll*180/3.14159,
                message.orientation.pitch*180/3.14159)
    drawDepth(message.position.depth,
              message.altitude);
    drawVelocity(message.body_velocity.x);
});

// THRUSTERS DATA
var listenerSP = new ROSLIB.Topic({
    ros : ros,
    name : '/cola2_control/thrusters_data_hz',
    messageType : 'cola2_msgs/Setpoints'
});

listenerSP.subscribe( function(message) {
    drawSetpoints( message.setpoints );
});


// SIDE SCAN SONAR
var listenerSSS = new ROSLIB.Topic({
    ros : ros,
    name : '/sss_data',
    messageType : 'cola2_msgs/ImagenexSSS'
});

listenerSSS.subscribe( function(message) {
    //alert(JSON.stringify(message))
    drawSSS( message.port_channel.concat(message.starboard_channel), "sss_canvas" );
});


// MULTIBEAM
var listenerMBS = new ROSLIB.Topic({
    ros : ros,
    name : '/multibeam_hz',
    messageType : 'sensor_msgs/LaserScan'
});

listenerMBS.subscribe( function(message) {
    drawLaserScan( message.ranges,
                   message.intensities,
                   message.angle_min,
                   message.angle_increment,
                   message.range_max,
                   "multibeam_canvas" );
});


// PROFILER/MICRON
var listenerMBS = new ROSLIB.Topic({
    ros : ros,
    name : '/profiler_hz',
    messageType : 'sensor_msgs/LaserScan'
});

listenerMBS.subscribe( function(message) {
    drawLaserScan( message.ranges,
                   message.intensities,
                   message.angle_min,
                   message.angle_increment,
                   message.range_max,
                   "profiler_canvas" );
});


// MISSION TIMEOUT
var total_timeout = 3600;

var timeout = new ROSLIB.Param({
    ros : ros,
    name : '/safety/timeout'
});

var listenerUpTime = new ROSLIB.Topic({
    ros : ros,
    name : '/cola2_safety/total_time',
    messageType : 'cola2_msgs/TotalTime'
});

listenerUpTime.subscribe( function(message) {
    timeout.get(function(value) {
        total_timeout = value;
    } );

    drawTimeout( message.total_time,  total_timeout);
});


// DIAGNOSTICS
var listener_diagnostic = new ROSLIB.Topic({
    ros : ros,
    name : '/diagnostics_agg',
    messageType : 'diagnostic_msgs/DiagnosticArray'
});

listener_diagnostic.subscribe(function(message) {

    // Draw battery Charge from diagnostics
    for( var i = 0; i < message.status.length; i++ ) {
        if( message.status[i].name == '/safety/ battery' ) {
            for( var j = 0; j < message.status[i].values.length; j++ ) {
                if (message.status[i].values[j].key == 'charge') {
                    drawCharge(parseFloat(message.status[i].values[j].value));
                }
            }
        }
    }

    // General check for diagnostics
    var status = 0
    var errors = '';
    for(var i = 0; i < message.status.length; i++) {

        if (status < 2 && message.status[i].level == 1) {
            status = 1;
            errors = errors + ' ' + message.status[i].message;
        }
        if (message.status[i].level == 2) {
            errors = errors + ' ' + message.status[i].message;
            status = 2;
        }

        if ( status == 0 )  {
            document.getElementById("diagnostic_message").style.background = green;
            document.getElementById("diagnostic_message").innerHTML = 'Diagnostics Ok';
        }
        else if ( status == 1 ) {
            document.getElementById("diagnostic_message").style.background = orange;
            document.getElementById("diagnostic_message").innerHTML = errors;
        }
        else {
            document.getElementById("diagnostic_message").style.background = red;
            document.getElementById("diagnostic_message").innerHTML = errors;
        }

    }
});

  // Calling services
  // -----------------

  // Enable GOTO
  var enable_goto = new ROSLIB.Service({
    ros : ros,
    name : '/cola2_control/enable_goto',
    serviceType : 'cola2_msgs/NewGoto'
  });

  // Set_Mode
  var set_mode = new ROSLIB.Service({
    ros : ros,
    name : '/mission_manager/set_mode',
    serviceType : 'cola2_msgs/SetMode'
  });

  // TILT
  var inc_tilt = new ROSLIB.Service({
    ros : ros,
    name : '/cola2_control/inc_tilt',
    serviceType : 'std_srvs/Empty'
  });

  var dec_tilt = new ROSLIB.Service({
    ros : ros,
    name : '/cola2_control/dec_tilt',
    serviceType : 'std_srvs/Empty'
  });

  var emptyRequest = new ROSLIB.ServiceRequest({});

  // Digital outputs
  var digital_output_srv = new ROSLIB.Service({
    ros : ros,
    name : '/digital_output',
    serviceType : 'cola2_msgs/DigitalOutput'
  });

 var mainlightsOn = new ROSLIB.ServiceRequest({
    digital_out : 4,
    value : true
  });

 var mainlightsOff = new ROSLIB.ServiceRequest({
    digital_out : 4,
    value : false
  });


 var lightsOn = new ROSLIB.ServiceRequest({
    digital_out : 13,
    value : true
  });

 var lightsOff = new ROSLIB.ServiceRequest({
    digital_out : 13,
    value : false
  });

 var CameraOn = new ROSLIB.ServiceRequest({
    digital_out : 7,
    value : true
  });

 var CameraOff = new ROSLIB.ServiceRequest({
    digital_out : 7,
    value : false
  });

 var pumpEstribordOn = new ROSLIB.ServiceRequest({
    digital_out : 4,
    value : true
  });

var pumpEstribordOff = new ROSLIB.ServiceRequest({
    digital_out : 4,
    value : false
  });

 var pumpBabordOn = new ROSLIB.ServiceRequest({
    digital_out : 9,
    value : true
  });

var pumpBabordOff = new ROSLIB.ServiceRequest({
    digital_out : 9,
    value : false
  });


//FUNCTIONS
function callEnableGoto() {
    var _x = parseFloat(document.getElementById("position_x").value);
    var _y = parseFloat(document.getElementById("position_y").value);
    var _z = parseFloat(document.getElementById("position_z").value);
    var _altitude_mode = document.getElementById("altitude_mode").checked;
    var _reference = parseInt(document.getElementById("reference").value);
    var _tx = parseFloat(document.getElementById("tolerance_x").value);
    var _ty = parseFloat(document.getElementById("tolerance_y").value);
    var _tz = parseFloat(document.getElementById("tolerance_z").value);

    console.log("Call service enable_goto " + _x + ", " + _y + ", " + _z + " mode: " + _altitude_mode + ", reference: " + _reference);
    var params = new ROSLIB.ServiceRequest({
       position: {
           x: _x,
           y: _y,
           z: _z
       },
       yaw: 0.0,
       altitude: _z,
       altitude_mode: _altitude_mode,
       blocking: false,
       priority: 10,
       reference: _reference,
       disable_axis: {
           x: false,
           y: true,
           z: false,
           roll: true,
           pitch: true,
           yaw: false
       },
       position_tolerance: {
           x: _tx,
           y: _tx,
           z: _tz
       },
       orientation_tolerance: {
           roll: 0.5,
           pitch: 0.5,
           yaw: 0.5
       }
   });
   enable_goto.callService(params, function(result){console.log(result);});
}

function callSetMode() {

    var _set_mode = parseInt(document.getElementById("swim_mode").value);

    console.log("Call service set_mode ");
    var params = new ROSLIB.ServiceRequest({
       mode_status : _set_mode
    });
    set_mode.callService(params, function(result){console.log(result);});
}

function callIncTilt() {
  console.log( "Call Inc Tilt service: " + inc_tilt.name );
  inc_tilt.callService( emptyRequest, function(result) { console.log( result ); } );
}

function callDecTilt() {
  console.log( "Call Dec Tilt service: " + dec_tilt.name );
  dec_tilt.callService( emptyRequest, function(result) { console.log( result ); } );
}

function callMainLightsOn() {
    console.log( "Call main lights on service: " + digital_output_srv.name + " --> " + mainlightsOn );
    digital_output_srv.callService(mainlightsOn, function(result) { console.log( result.success ); })
}

function callMainLightsOff() {
    console.log( "Call main lights off service: " + digital_output_srv.name + " --> " + mainlightsOff );
    digital_output_srv.callService(mainlightsOff, function(result) { console.log( result.success ); })
}


function callLightsOn() {
    console.log( "Call lights on service: " + digital_output_srv.name + " --> " + lightsOn );
    digital_output_srv.callService(lightsOn, function(result) { console.log( result.success ); })
}

function callLightsOff() {
    console.log( "Call lights off service: " + digital_output_srv.name + " --> " + lightsOff );
    digital_output_srv.callService(lightsOff, function(result) { console.log( result.success ); })
}

function callCameraOn() {
    console.log( "Call camera on service: " + digital_output_srv.name + " --> " + lightsOn );
    digital_output_srv.callService(CameraOn, function(result) { console.log( result.success ); })
}

function callCameraOff() {
    console.log( "Call camera off service: " + digital_output_srv.name + " --> " + lightsOff );
    digital_output_srv.callService(CameraOff, function(result) { console.log( result.success ); })
}

function callPumpEstribordOn() {
    console.log( "Call pump estribord on service: " + digital_output_srv.name + " --> " + pumpEstribordOn );
    digital_output_srv.callService(pumpEstribordOn, function(result) { console.log( result.success ); })
}

function callPumpEstribordOff() {
    console.log( "Call pump estribord off service: " + digital_output_srv.name + " --> " + pumpEstribordOff );
    digital_output_srv.callService(pumpEstribordOff, function(result) { console.log( result.success ); })
}

function callPumpBabordOn() {
    console.log( "Call pump babord on service: " + digital_output_srv.name + " --> " + pumpBabordOn );
    digital_output_srv.callService(pumpBabordOn, function(result) { console.log( result.success ); })
}

function callPumpBabordOff() {
    console.log( "Call pump babord off service: " + digital_output_srv.name + " --> " + pumpBabordOff );
    digital_output_srv.callService(pumpBabordOff, function(result) { console.log( result.success ); })
}
