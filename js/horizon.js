// inner variables
var canvas, ctx;

// draw functions :
function clear() { // clear canvas function
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function drawHorizon( roll, pitch ) { // main drawScene function
   
    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    
    canvas = document.getElementById('horizon_canvas');
    ctx = canvas.getContext('2d');
    clear(); // clear canvas

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var compass_radius = Math.min(width, height)/2.1;

    ctx.save();
    var roll = (roll+90)/6;
    
    // draw circle (as background)
    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/2.5, 0, 2 * Math.PI, false);
    ctx.lineWidth = 3;
    ctx.strokeStyle = black;
    ctx.stroke();

    // draw numbers
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.beginPath();
    ctx.font = ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
    ctx.fillStyle = black;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    values=['-30º', '-15º', '0º', '15º', '30º', '45º', '-30º', '-15º', '0º', '15º', '30º', '45º', ]
    for (var n = 1; n <= 12; n++) {
        var theta = (n - 3) * (Math.PI * 2) / 12;
        var x = compass_radius * 0.65 * Math.cos(theta);
        var y = compass_radius * 0.65 * Math.sin(theta);
        ctx.fillText(values[n-1], x, y);
    }

    // draw roll
    ctx.save();
    var theta = (roll - 15) * 2 * Math.PI / 30;
    ctx.rotate(theta);
    ctx.beginPath();

    ctx.moveTo(0, -3);
    ctx.lineTo(0, 3);
    ctx.lineTo(compass_radius * 0.8, 1);
    ctx.lineTo(compass_radius * 0.8, -1);
    ctx.moveTo(0, -3);
    ctx.lineTo(0, 3);
    ctx.lineTo(-compass_radius * 0.8, 1);
    ctx.lineTo(-compass_radius * 0.8, -1);

    ctx.fillStyle = red;
    ctx.fill();
    ctx.restore();
    ctx.restore();

    // Draw pitch lines
    ctx.save();
    ctx.fillStyle = grey;
    ctx.translate(0, -1.5*height + Math.round((0.04*pitch)*height));

    pitch_values = ['45º', '40º', '35º', '30º', '25º', '20º', '15º', '10º', '5º', '0º', '-5º', '-10º', '-15º', '-20º', '-25º', '-30º', '-35º', '-40º', '-45º']

    for( var i = 1; i < 20; i++ ) {
        ctx.beginPath();
        ctx.strokeStyle = grey;
        h = (height/5)*i
        ctx.moveTo(0, h);
        ctx.lineTo(width, h);
        ctx.stroke();
        ctx.fillText(pitch_values[i-1], 0, Math.floor(h-2));
    }
    ctx.restore();
 
}

